module.exports = {
    content: ["./src/**/*.{html,js}", "templates/**/*.html.twig", "assets/js/**/*.js", "assets/js/**/*.vue", "assets/js/**/*.ts"],
    theme: {
        extend: {},
    },
    plugins: [require("@tailwindcss/forms")],
};
