<?php

namespace App\Tests;

use App\Entity\Project;
use PHPUnit\Framework\TestCase;

class ProjectTest extends TestCase
{
    public function testCreateProject(): void
    {
        $project = new Project();

        $project->setTitle('Test Project');

        $this->assertSame('Test Project', $project->getTitle());
    }
}

