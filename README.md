# Slein Devops Project - Docker Symfony 6 starter kit

This development stack includes Symfony 6/Webpack Encore, Typescript, VueJS, SCSS, TailwindCSS, MySQL, Apache and PHP built with Docker containers using docker-compose tool.
Includes Gitlab CI / CD process for dev environment

## Services

-   MySQL 8.0
-   Apache
-   PHP8-FPM 8.1
-   NodeJS 16.0

## Installation

1. Clone the repository, build services and create/Start containers:

```sh
$ cd <project-name>
$ docker-compose build
$ docker-compose up -d
```

2. Visit http://127.0.0.1/ or http://localhost

3. Install additional frontend dependencies and watch for changes:

```sh
$ docker-compose exec php yarn
$ docker-compose exec php yarn watch
```

OR

```sh
$ docker-compose exec php npm
$ docker-compose exec php npm watch
```

## Folders structure

```text
slein-devops-project/
├─ .docker/
│ ├─ apache/
│ │   ├─ config/
│ │   └─ Dockerfile
│ ├─ mysql/
│ │   └─ config/
│ └─ php/
│     ├─ config/
│     │    └─ docker-entrypoint.sh
│     └─ Dockerfile
│ ...
│ ├─ .env
│ ├─ docker-compose.yml
│ └─ .gitlab-ci.yml
```

-   .docker : folder containing Dockerfiles and additional information to run containers. Here is also stored the data of mounted service VOLUME (database files, logs etc.).
-   .env : Symfony 6/Docker configuration file - feel free to change regarding your needs
-   docker-compose.yml : definition of multi-container Docker application
-   .gitlab-ci.yml : definition of CI / CD process Gitlab

## Troubleshooting

> Environment: WINDOWS 10
> Error: Windows Docker Error - standard_init_linux.go:211: exec user process caused "no such file or directory"
> Solution: Error link to CLRF EOL created after repo is pulled. In case of issue, please change the EOL from CRLF to LF in following files.
> (you can use Notepad++ and its option find/replace - \r\n to \n)

```sh
$ cd .docker/php/config/
$ .docker/php/config/Dockerfile
$ .docker/php/config/docker-entrypoint.sh
```

## Commands

```sh
# Docker
$ docker-compose up -d
$ docker-compose down
$ docker-compose up -d --no-deps --build mysql
$ docker-compose up -d --no-deps --build apache
$ docker-compose up -d --no-deps --build php
$ docker-compose exec php sh

# Symfony
$ docker-compose exec php php bin/console cache:clear

# Composer
$ docker-compose exec php composer install

# Yarn
$ docker-compose exec php yarn
$ docker-compose exec php yarn watch

# NPM
$ docker-compose exec php npm install
$ docker-compose exec php npm run watch
```

## Other

In this starter, Node and Yarn were installed directly in PHP container. However, NodeJS can be swiftly defined as separate service (for React/Vue frontend application etc.) as follow:

```diff
# docker-compose.yml

version: '3.8'
services:
    ...
+  nodejs:
+    build:
+      context: .
+      dockerfile: ./.docker/nodejs/Dockerfile
+    environment:
+      PHP_HOST: php
+      PHP_PORT: 9000
+    volumes:
+      - .:/var/www/html:rw
+    depends_on:
+      - php
```

```diff
# .docker/php/config/Dockerfile

ARG PHP_VERSION=7.4
FROM php:${PHP_VERSION}-fpm-alpine
RUN apk add --update \
    zip \
    unzip \
    curl \
-    nodejs \
-    yarn
...
```

## Notice

Stack was configured for developments purposes. Please do not consider deploying it on production as is, without previous review.

# slein-devops-project

### Made with love By Slein.
