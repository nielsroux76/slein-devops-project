import { createApp } from "vue";

import Bordereau from "../components/forms/Bordereau.vue";

createApp(Bordereau).mount("#app");
