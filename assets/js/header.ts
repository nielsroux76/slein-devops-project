import { createApp } from "vue";
import Header from "./components/Header.vue";

createApp(Header).mount("#header");
